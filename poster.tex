\documentclass{beamer}
\usepackage[orientation=portrait,size=a0]{beamerposter}
\mode<presentation>{\usetheme{ZH}}
% \usepackage{chemformula}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
% \usepackage[german, english]{babel} % required for rendering German special characters
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[autoprint=false, gobble=auto]{pythontex} % create figures on-line directly from python!
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage[font=scriptsize,justification=justified]{caption}
\usepackage{amsmath}
\usepackage{mathtools}
\DeclareMathOperator{\cov}{Cov}
\DeclareMathOperator{\var}{Var}

% \sisetup{per=frac,fraction=sfrac}
\sisetup{per-mode=symbol}

\input{pythontex/functions.tex}
\begin{pythontexcustomcode}[begin]{py}
DOC_STYLE = 'poster/main.conf'
pytex.add_dependencies(DOC_STYLE, 'poster/wide.conf', 'poster/synthetic.conf', 'poster/correlation.conf', 'poster/tvalues.conf', 'poster/3dplot.conf')
\end{pythontexcustomcode}

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns

\title{Contrast Optimized Stimulus generator (COSgen)}
% \title{Longitudinal opto-pharmaco-fMRI of Selective\\ Serotonin Reuptake Inhibition}
\author{Florian Aymanns$^{1}$, Horea-Ioan Ioanas$^{1}$, Markus Rudin$^{1}$}
\institute[ETH]{$^{1}$Institute for Biomedical Engineering, ETH and University of Zurich}
\date{\today}

\newlength{\columnheight}
\setlength{\columnheight}{0.881\textheight}

\begin{document}
\begin{frame}
\begin{columns}
	\begin{column}{.43\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Introduction}
						As fMRI measurements are complex, expensive, and often target low-amplitude effects, the statistical optimization of stimulus sequences for specific experimental targets plays an integral role in the experiment design process.
						We provide an extremely flexible framework to perform such optimizations using the genetic algorithm (GA) introduced by Wager and Nichols ~\cite{WagerNichols} and improved by Kao et al. ~\cite{Kao}.
						Our Python implementation improves on many aspects of existing solutions, including higher parameterization, API availability, and detailed documentation.
						Because of its modular structure, our implementation is highly adaptable to specific use cases in both human and animal fMRI.
						Nevertheless, we also provide an easy to use default algorithm, with widely applicable and properly cited presets.
						The implementation allows full model (design matrix construction and covariance matrix computation) as well as fitness measure specification.
					\end{myblock}\vfill
					\begin{myblock}{Mathematical Foundation}
						\vspace{1ex}
						\textbf{GLM}:\\
						\vspace{1ex}
						$Y=X\beta+\epsilon$,\\ where $X$ is the design matrix, $Y$ a vector of random variables and $\epsilon$ is noise\\
						\vspace{2ex}
						\textbf{Best linear unbiased estimator (BLUE)}:\\$\hat{\beta}=(X'X)^{-1}X'y$\\
						Assumptions:
						\begin{itemize}
							\begin{multicols}{3}
								\item $\mathbb{E}[\epsilon_i]=0$
								\item $\var(\epsilon_i)=\sigma^2$
								\textcolor{gray}{\item $\cov(\epsilon_i,\epsilon_j)=0, \forall i \neq j$}
							\end{multicols}
						\end{itemize}
						The last assumption is generally not fulfilled for fMRI data unless it is whitened.\\
						\vspace{2ex}
						\textbf{How to Measure the Fitness of a Sequence?\\}
						\vspace{1ex}
						\textit{Idea}: Minimize variance of parameter estimates $\hat{\beta}$\\
						\vspace{1ex}
						\textbf{Covariance matrix}:\\
						$\cov(\hat{\beta})_{ij} \coloneqq \mathbb{E}[(\hat{\beta}_i-\beta_i)(\hat{\beta}_j-\beta_j))]$\quad
						$\cov(\hat{\beta})=(X'X)^{-1}X'\cov(\epsilon)X(X'X)^{-1}=X^+\cov(\epsilon)(X^+)'$\\
						\vspace{1ex}
						Examples for fitness measures: $\text{tr}(\cov(\hat{\beta}))$, $\det(\cov(\hat{\beta}))$\\
						For details see Chapter 15 of ~\cite{spm}.
					\end{myblock}\vfill
					\begin{myblock}{Synthetic Data}
						In order to validate the results of the algorithm synthetic data was used.
						The data is based on a $1500s$ long resting state scan of a mouse brain.
						Activation is estimated and added to the time course of every voxel in the region of interest.
						The simplest estimate for the activation is the convolution of the haemodynamic response function (HRF) and the stimulus sequence.
						More complex estimations take non-linearity effects into account using Volterra series~\cite{Friston1998}.
						\vspace{0.6em}
						\begin{center}
						\py{pytex_fig('scripts/synthetic_data.py', conf='poster/synthetic.conf', label='syntheticdata', caption='Computation of synthetic data for the time course of one voxel. \textit{Left}: Time course of one voxel (noise) \textit{Middle}: Assumed activation (convolution of sequence and HRF) \textit{Right}: Sum of noise and activation (synthetic time course)')}
						\begin{figure}
							\includegraphics[scale=1.5]{img/mask.pdf}
							\caption{Region of interest used for the synthetic data. It is the cortex of a mouse brain.}
							\label{fig:mask}
						\end{figure}
						\end{center}
						\vspace{-1em}
					\end{myblock}\vfill
					\begin{myblock}{Validation of fitness measure}
						Theoretically, the fitness measures $\text{tr}(\cov(\hat{\beta}))$ and $\det(\cov(\hat{\beta}))$ should directly correspond to the sensitivity index $d' \coloneqq Z(\text{true positive rate}) - Z(\text{false positive rate})$, where $Z$ is the inverse of the cumulative Gaussian distribution.
						However, there are a lot of parameters (noise whitening, non-linearity, HRF-shape) that have to be taken into account.
						Figure \ref{fig:correlation} shows that the fitness measures predicts the sensitivity index calculated form the synthetic data for the parameters chosen.
						The analysis was done using SAMRI~\cite{SAMRI} a Python wrapper for Nipype specifically suited for small animal data.
						\py{pytex_fig('scripts/plot_block_opt.py', conf='poster/correlation.conf', label='correlation', caption='Correlation of theoretical fitness measure and sensitivity index of synthetic data.')}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.57\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Results}
						It is documented that block designs tend to be optimal for contrast detection (c.f. Chapter 8 of ~\cite{spm}).
						In accordance with that our algorithm found that a sequence with $196s$ blocks is optimal given the custom HRF used in Figure \ref{fig:syntheticdata}.
						However, for some stimulus types such long blocks can result in habituation --- or even tissue damage --- and therefore should be avoided.
						We restricted our search to blocks of length $5s$ to $40s$ and rest intervals up to $200s$.
						Figure \ref{fig:3dplot} shows that the best fitness is achieved with $40s$ stimulus blocks and $91s$ rest in between.
						A plot of the optimal sequence can be found in Figure \ref{fig:sequence}.
						The t-values in the region of interest are significantly higher for the optimized sequence compared to original sequence, i.e. the ability to detect the signal is higher.\\
						\begin{minipage}{0.48\textwidth}
							\py{pytex_fig('scripts/plot_tvalues.py', conf='poster/tvalues.conf', label='tvalues', caption='Comparison of the t-value distributions of synthetic data generated using the original sequence and the optimized sequence. The green histogram are the values in the region of interest and blue are the remaining voxels.')}
						\end{minipage}
						\hspace{1em}
						\begin{minipage}{0.48\textwidth}
							\py{pytex_fig('scripts/3dplot.py', conf='poster/3dplot.conf', label='3dplot', caption='Fitness for different stimulus duration and different resting periods (gaps) in between. The black dots show the optimal resting interval for a given stimulus block length.')}
						\end{minipage}
						\py{pytex_fig('scripts/sequence.py', conf='poster/sequence.conf', label='sequence', caption='Optimized block style sequence.')}
					\end{myblock}
					\begin{myblock}{Outlook}
						\begin{itemize}
							\item Improve GA initial population and choice of immigrants to allow independent stimulus block and resting period optimization. 
							\item Confirm results with wet work experiments in mice and/or rats  with optogenetic and electric stimulation.
							\item Improve handling of non-linearity $\rightarrow$ Find better estimate of Volterra kernels.
							\item Reduce dependency on HRF.
						\end{itemize}
					\end{myblock}\vfill
					\begin{myblock}{Contrast Optimized Stimulus player (COSplay)}
						COSplay is a MicroPython/Pyboard based delivery system for BIDS-formatted~\cite{Gorgolewski2016} sequences --- including those generated with COSgen.
						The system receives a TTL pulse trigger signal from the MRI machine and then delivers a sequence of stimuli.
						It uses TTL pulses or short circuiting to steer stimulation devices such as lasers for optogenetic stimulation or electric stimulation boxes for sensory stimulation.
						\begin{itemize}
						\item In total 6 different stimulation devices can be connected simultaneously.
						\item Two of the output channels can deliver analogue pulses of varying amplitude.
						\item The stimuli are delivered with an accuracy up to $1 \mu s$.
						\item Furthermore we developed a small program that can display the status of the system on the computer of the MRI machine and store delivered sequences with the scan data.
						\end{itemize}
						\begin{figure}
							\begin{minipage}{0.43\textwidth}
								\centering\includegraphics[width=0.85\textwidth]{img/newsystem.jpg}
								\caption{Python based system for delivery of stimuli.}
							\end{minipage}
							\hspace{1em}
							\begin{minipage}{0.43\textwidth}
								\centering\includegraphics[width=0.85\textwidth]{img/workflow.pdf}
								\caption{Workflow.}
							\end{minipage}
						\end{figure}
					\end{myblock}\vfill
					\begin{myblock}{References}
						\begin{multicols}{3}
						\footnotesize
						%\scriptsize
						\bibliographystyle{abbrv}
						\bibliography{./bib}
						\end{multicols}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\end{frame}
\end{document}
