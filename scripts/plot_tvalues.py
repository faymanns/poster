#!/usr/bin/env python

from matplotlib import pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy.stats import norm, lognorm, frechet_l, t


"""t dist"""
f, axarr = plt.subplots(2, sharex=True)

data1_remain = np.load('data/originalremaintval.npy')
idx_notnan = np.where(np.isnan(data1_remain)==False)
n1, bins1, patches1 = axarr[0].hist(data1_remain[idx_notnan],'auto',normed=1, facecolor='blue', alpha=0.75)

data1 = np.load('data/originalroitval.npy')
df1, loc1, scale1 = t.fit(data1,len(data1)-1)
#print(t._fitstart(data1))
#print(loc1, scale1)
n1, bins1, patches1 = axarr[0].hist(data1,'auto',normed=1, facecolor='green', alpha=0.75)
y1 = t.pdf(bins1, df1, loc1, scale1)

data2_remain = np.load('data/optimizedremaintval.npy')
idx_notnan = np.where(np.isnan(data2_remain)==False)
n2, bins2, patches2 = axarr[1].hist(data2_remain[idx_notnan],'auto',normed=1, facecolor='blue', alpha=0.75)

data2 = np.load('data/optimizedroitval.npy')
df2, loc2, scale2 = t.fit(data2, len(data2)-1)
#print(t._fitstart(data2))
#print(loc2, scale2)
(mu2, sigma2) = norm.fit(data2)
n2, bins2, patches2 = axarr[1].hist(data2,'auto',normed=1,facecolor='green',alpha=0.75)
y2 = t.pdf(bins2, len(data2)-1, loc2, scale2)

axarr[0].plot(bins1, y1, 'r--', linewidth=2)
#axarr[0].set_title(r'Original sequence $\mathrm{(\mu=%.3f,\ \nu=%.3f}$)' %(loc1, df1))
axarr[0].set_title(r'Original sequence $\mathrm{(\mu=%.3f}$)' %(loc1),fontsize=30)
axarr[1].plot(bins2, y2, 'r--', linewidth=2)
axarr[1].set_title(r'Optimized sequence $\mathrm{(\mu=%.3f}$)' %(loc2),fontsize=30)
axarr[1].set_xlabel('t-values',fontsize=30)
#plt.savefig(save_as)
