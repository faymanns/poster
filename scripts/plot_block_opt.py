import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import norm




"""196"""
start=5

f,ax = plt.subplots()
#plt.subplots_adjust(hspace=1.2)
x=np.arange(180,210,1)[start:]

aopt = np.load('data/aopt-fitnesses196.npy')[start:]
a, = ax.plot(x,aopt,'b-')
#ax.set_title('Fitness measure')
ax.set_xlabel('Block length',fontsize=30)
ax.set_ylabel('Fitness in arb. units',color='b',fontsize=30)
ax.tick_params('y', colors='b')

fp = np.load('data/fps196.npy')[start:]
totalp = np.load('data/totalp196.npy')[start:]
tp = np.load('data/tps196.npy')[start:]
fn = np.load('data/fns196.npy')[start:]
tn = 65522 - fp - tp - fn
fprate = fp/(fp + tn)
tprate = tp/(tp + fn)
dprime = norm.ppf(tprate) - norm.ppf(fprate)

ax2=ax.twinx()
d, = ax2.plot(x,dprime,'g-')
#ax2.set_title("Sensitivity index")
#ax2.set_xlabel('Block length')
ax2.set_ylabel("d'",color='g',fontsize=30)
ax2.tick_params('y', colors='g')


#plt.savefig('fitnessm_corr196.pdf', bbox_inches='tight')
#plt.show()
