from cosgen.function_crate import FunctionCrate
from cosgen.fitness_measures import estimator_variance
from cosgen.sequence import Sequence
import cosgen.models

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.widgets import Slider
import functools
from scipy.ndimage.filters import gaussian_filter1d
from matplotlib import rcParams
import matplotlib.colors as colors
facecolor = colors.hex2color(rcParams['axes.facecolor'])
rcParams['axes.facecolor'] = 'FFFFFF'

seql = 1490

f = np.load('data/bestblocksize_cutoff_volterra05_gap.npz')
intervalblock =f['arr_0']
intervalgap =f['arr_1']
blockfitnesses =np.matrix(f['arr_2'])
X, Y = np.meshgrid(intervalblock, intervalgap)
#fig = plt.figure(figsize=(10,10))
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(X,Y,blockfitnesses,cmap=cm.hot,alpha=0.8)
ax.w_xaxis.set_pane_color(facecolor)
ax.w_yaxis.set_pane_color(facecolor)
ax.w_zaxis.set_pane_color(facecolor)
a = np.array(blockfitnesses)
gapmax = []
for i in range(len(intervalblock)):
	index = np.argmax(a[:,i])
	#print(intervalblock[i],intervalgap[index],a[index,i])
	gapmax.append(intervalgap[index])
	ax.scatter(intervalblock[i],intervalgap[index],a[index,i]+0.1,c='black')
ax.set_xlabel("\n\nBlock size in s",fontsize=30)
ax.set_ylabel("\n\nGap size in s",fontsize=30)
ax.set_zlabel("\n\nFitness in arbirtry units",fontsize=30)
#ax.set_title("Block size efficiency spectrum")
ax.view_init(elev=30,azim=-135)
#plt.savefig('bestblocksize_cutoff_volterra05_gap.pdf', bbox_inches='tight')
#plt.show()

#coef = np.polyfit(intervalblock,gapmax,1)
#print(coef)
#p=np.poly1d(coef)
#plt.figure()
#m, = plt.plot(intervalblock,gapmax)
#f, = plt.plot(intervalblock,p(intervalblock))
#plt.title('Best rest duration')
#plt.xlabel('Stimulus length in s')
#plt.ylabel('Gap size in s')
#plt.savefig('bestrestdur.pdf',bbox_inches='tight')
#plt.show()

