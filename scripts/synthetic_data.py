import numpy as np
from matplotlib import pyplot as plt

voxel = np.load('data/voxel_time_course.npy')
seql = len(voxel)
hrf = np.load('data/hrf.npy')
seq = np.load('data/originalsequence.npy')

f,ax = plt.subplots(1,3,sharey=True)

minimum = min(voxel)
maximum = max(np.convolve(hrf,seq)[:seql]+voxel)

ax[0].set_xlim(0,seql)
ax[0].set_ylim(minimum,maximum)
ax[0].axis('off')
ax[0].plot(voxel)
ax[0].text(seql+40,0,'+',fontsize=40)
#plt.savefig('voxel_time_course.pdf', bbox_inches='tight')

ax[1].set_xlim(0,seql)
ax[1].set_ylim(minimum,maximum)
ax[1].axis('off')
ax[1].plot(np.convolve(hrf,seq)[:seql])
ax[1].text(seql+30,0,'=',fontsize=40)
#plt.savefig('seq_time_course.pdf', bbox_inches='tight')

ax[2].set_xlim(0,seql)
ax[2].set_ylim(minimum,maximum)
ax[2].axis('off')
ax[2].plot(np.convolve(hrf,seq)[:seql]+voxel)
#plt.savefig('added_time_course.pdf', bbox_inches='tight')
#plt.show()
